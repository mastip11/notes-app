//
//  EditNotesViewController.swift
//  To Do List App
//
//  Created by Tivo Yudha on 5/20/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import UIKit            

class EditNotesViewController: AddNotesViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        addButton?.setTitle("Edit Notes", for: .normal)
        setUpDataToUI(title: model?.title ?? "", message: model?.message ?? "")
        
        deleteButton.isHidden = false
    }
    
    override func onButtonClicked() {
        if (isValidate()) {
            model = TaskModel(id: model?.id ?? 0, title: titleTextField.text!, message: notesTextField.text!, date: Date())
            DataManager.updateData(model: model!)
            delegate?.sendDataBack(isEdit: true, model: model!)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func onDeleteClicked() {
        let deleteAlert = UIAlertController(title: "Delete Confirmation", message: "Do you want to delete model?", preferredStyle: .alert)
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {
            (action: UIAlertAction) in
            deleteAlert.dismiss(animated: true, completion: nil)
        }))
        deleteAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
            (action: UIAlertAction) in
            DataManager.deleteData(model: self.model!)
            self.delegate?.onDelete(model: self.model!)
            self.dismiss(animated: true, completion: nil)
        }))
        present(deleteAlert, animated: true, completion: nil)
    }
}
