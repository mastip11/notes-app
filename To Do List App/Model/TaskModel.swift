//
//  TaskModel.swift
//  To Do List App
//
//  Created by Tivo Yudha on 10/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import Foundation

/**
    class that similar to data class in kotlin
 **/
struct TaskModel {
    var id: Int = 0
    var title: String = ""
    var message: String = ""
    var date: Date = Date()
}
