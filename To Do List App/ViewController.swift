//
//  ViewController.swift
//  To Do List App
//
//  Created by Tivo Yudha on 08/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NotesDelegate, TaskTableViewCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabelView: UILabel!
    
    private var toDoList = [TaskModel]()
    
    /**
        similar like onCreate in android
    **/
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        
        addRefreshView()
        
        tableView.register(UINib(nibName: "TaskTableViewCell", bundle: nil), forCellReuseIdentifier: "TaskTableViewCell")
        loadDataFromServer()
    }
    
    /**
        function for set up view cell
    **/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = toDoList[indexPath.row]
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "TaskTableViewCell", for: indexPath) as! TaskTableViewCell
        cell.delegate = self
        cell.setUpDataToUI(model: model)
        
        let animation = AnimationHelper.makeMoveUpWithBounce(rowHeight: cell.frame.height, duration: 1, delayFactor: 0.05)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: self.tableView)
        
        return cell
    }
    
    /**
        function for measuring height of cell
     **/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /**
        function for return count of data in list
    **/
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onOpenNotesViewController(model: toDoList[indexPath.row])
    }
    
    /**
        function will trigger when add button is clicked
    **/
    @IBAction func onAddButtonClicked(_ sender: Any) {
        onOpenNotesViewController(model: nil)
    }
    
    private func reloadData() {
        toDoList = DataManager.getData()
        
        if (toDoList.count == 0) {
            noDataLabelView.isHidden = false
        } else {
            noDataLabelView.isHidden = true
        }
        
        tableView.refreshControl?.endRefreshing()
        tableView.reloadData()
    }
    
    private func onOpenNotesViewController(model: TaskModel?) {
        var vc = AddNotesViewController()
        if (model != nil) {
            vc = EditNotesViewController()
            vc.model = model
        }
        
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func onDelete(model: TaskModel) {
        reloadData()
    }
    
    /**
        delegate for receive data from AddNotesViwController
     **/
    func sendDataBack(isEdit: Bool, model: TaskModel) {
        reloadData()
    }
    
    func onLongPressed(model: TaskModel) {
        let deleteDialog = UIAlertController(title: "Delete Confirmation", message: "Do you want to delete this task?", preferredStyle: .alert)
        deleteDialog.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {
            (action: UIAlertAction) in
            deleteDialog.dismiss(animated: true, completion: nil)
        }))
        deleteDialog.addAction(UIAlertAction(title: "Yes", style: .default, handler: {
            (action: UIAlertAction) in
            DataManager.deleteData(model: model)
            self.onDelete(model: model)
        }))
        present(deleteDialog, animated: true, completion: nil)
    }
    
    func loadDataFromServer() {
        TaskConnectionManager.getTaskFromServer(success: { taskModelList in
            DataManager.deleteAllData()
            
            taskModelList.forEach {
                DataManager.saveData(model: $0)
            }
            
            self.reloadData()
        }, failed: { message in
            print(message)
        })
    }
    
    private func addRefreshView() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshUI), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc private func refreshUI() {
        loadDataFromServer()
    }
}

