//
//  TaskTableViewCell.swift
//  To Do List App
//
//  Created by Tivo Yudha on 10/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    private let formatter = DateFormatter()
    private var model: TaskModel? = nil
    var delegate: TaskTableViewCellDelegate? = nil
    
    override func awakeFromNib() {
        addOnLongPressedGesture()
    }
    
    private func addOnLongPressedGesture() {
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(onLongPressedCell(sender:)))
        self.addGestureRecognizer(longPressGesture)
    }
    
    func setUpDataToUI(model: TaskModel) {
        self.model = model
        formatter.dateFormat = "dd MMMM yyyy"
        
        titleLabel?.text = model.title
        messageLabel?.text = model.message
        dateLabel?.text = formatter.string(from: model.date)
    }
    
    @objc func onLongPressedCell(sender: UILongPressGestureRecognizer) {
        delegate?.onLongPressed(model: model!)
    }
}
