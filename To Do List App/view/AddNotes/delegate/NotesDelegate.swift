//
//  AddNotesDelegate.swift
//  To Do List App
//
//  Created by Tivo Yudha on 17/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import Foundation

/**
    delegate for throw back the data to home
 **/
protocol NotesDelegate {
    func sendDataBack(isEdit: Bool, model: TaskModel)
    func onDelete(model: TaskModel)
}
