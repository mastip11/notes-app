//
//  AddNotesViewController.swift
//  To Do List App
//
//  Created by Tivo Yudha on 10/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import UIKit

class AddNotesViewController: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var notesTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    // variable for set delegate, the function are same
    var delegate: NotesDelegate? = nil
    var model: TaskModel? = nil
    
    public init() {
        super.init(nibName: "AddNotesViewController", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        deleteButton.isHidden = true
    }
    
    @IBAction func onAddNotesClicked(_ sender: Any) {
        onButtonClicked()
    }
    
    @IBAction func onDeleteNoteClicked(_ sender: Any) {
        onDeleteClicked()
    }
    
    func setUpDataToUI(title: String, message: String) {
        titleTextField?.text = title
        notesTextField?.text = message
    }
    
    /**
        function for validating notes text field, so the field cannot empty
     **/
    open func isValidate() -> Bool {
        return !titleTextField.text!.isEmpty || !notesTextField.text!.isEmpty
    }
    
    open func onButtonClicked() {
        if (isValidate()) {
            model = TaskModel(id: DataManager.getData().count + 1, title: titleTextField.text!, message: notesTextField.text!, date: Date())
            DataManager.saveData(model: model!)
            delegate?.sendDataBack(isEdit: false, model: model!)
            
            // the function is just same like finish() in android
            self.dismiss(animated: true, completion: nil)
            //            self.navigationController?.popViewController(animated: true)
        }
    }
    
    open func onDeleteClicked() {
        
    }
}
