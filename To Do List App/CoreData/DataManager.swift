//
//  DataManager.swift
//  To Do List App
//
//  Created by Tivo Yudha on 28/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataManager {
    
    static var managedContext: NSManagedObjectContext? = nil

    static func getData() -> [TaskModel] {
        var taskList = [TaskModel]()
        
        do {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }
            let manageContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TaskEntity")
            
            let result = try manageContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                let taskModel = TaskModel.init(id: data.value(forKey: "id") as? Int ?? 0,
                                               title: data.value(forKey: "title") as? String ?? "",
                                               message: data.value(forKey: "message") as? String ?? "",
                                               date: data.value(forKey: "time") as? Date ?? Date())
                taskList.append(taskModel)
            }
        } catch {
            print("Error")
        }
        return taskList
    }
    
    static func saveData(model: TaskModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let manageContext = appDelegate.persistentContainer.viewContext
        let taskEntity = NSEntityDescription.entity(forEntityName: "TaskEntity", in: manageContext)
        
        let task = NSManagedObject(entity: taskEntity!, insertInto: manageContext)
        task.setValue(model.id, forKey: "id")
        task.setValue(model.title, forKey: "title")
        task.setValue(model.message, forKey: "message")
        task.setValue(model.date, forKey: "time")
        
        do {
            try manageContext.save()
        } catch {
            print("Error when save")
        }
    }
    
    static func updateData(model: TaskModel) {
        do {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let manageContext = appDelegate.persistentContainer.viewContext
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "TaskEntity")
            fetchRequest.predicate = NSPredicate(format: "id = %i", model.id)
            
            do {
                let fetchData = try manageContext.fetch(fetchRequest)
                
                if (fetchData.count > 0) {
                    let objectUpdate = fetchData[0] as! NSManagedObject
                    objectUpdate.setValue(model.title, forKey: "title")
                    objectUpdate.setValue(model.message, forKey: "message")
                    
                    do {
                        try manageContext.save()
                    } catch {
                        print("Failed to update data")
                    }
                } else {
                    print("No data found")
                }
            }
        } catch {
            print("Error on update data")
        }
    }
    
    static func deleteData(model: TaskModel) {
        do {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let manageContext = appDelegate.persistentContainer.viewContext
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "TaskEntity")
            fetchRequest.predicate = NSPredicate(format: "id = %i", model.id)
            
            do {
                let fetchData = try manageContext.fetch(fetchRequest)
                
                if (fetchData.count > 0) {
                    let objectUpdate = fetchData[0] as! NSManagedObject
                    manageContext.delete(objectUpdate)
                    
                    do {
                        try manageContext.save()
                    } catch {
                        print("Failed to update data")
                    }
                } else {
                    print("No data found")
                }
            }
        } catch {
            print("Error on update data")
        }
    }
    
    static func deleteAllData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let manageContext = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "TaskEntity")
        
        do {
            let fetchData = try manageContext.fetch(fetchRequest)
            
            if (fetchData.count > 0) {
                fetchData.forEach {
                    let objectDelete = $0 as! NSManagedObject
                    manageContext.delete(objectDelete)
                }
                
                do {
                    try manageContext.save()
                } catch {
                    print("Failed to save delete data")
                }
            } else {
                print("No data found")
            }
        } catch {
            print("Error delete all data")
        }
    }
}
