//
//  TableViewExtension.swift
//  To Do List App
//
//  Created by Tivo Yudha on 23/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleRows?.last else {
            return false
        }
        
        return lastIndexPath == indexPath
    }
}
