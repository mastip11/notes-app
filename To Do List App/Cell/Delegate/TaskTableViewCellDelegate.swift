//
//  TaskTableViewCellDelegate.swift
//  To Do List App
//
//  Created by Tivo Yudha on 23/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import Foundation

protocol TaskTableViewCellDelegate {
    func onLongPressed(model: TaskModel)
}
