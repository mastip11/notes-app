//
//  Animation.swift
//  To Do List App
//
//  Created by Tivo Yudha on 23/05/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import Foundation
import UIKit

typealias Animation = (UITableViewCell, IndexPath, UITableView) -> Void
