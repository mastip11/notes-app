//
//  TaskConnectionManager.swift
//  To Do List App
//
//  Created by Tivo Yudha on 17/06/19.
//  Copyright © 2019 Tivo Yudha. All rights reserved.
//

import Foundation
import Alamofire

class TaskConnectionManager {
    
    static func getTaskFromServer(success successHandler: @escaping (_ taskModelList: [TaskModel]) -> Void, failed failedHandler: @escaping (_ message: String) -> Void) {
        Alamofire.request("https://next.json-generator.com/api/json/get/E1UtrQx1D", method: .post).responseJSON { response in
            do {
                let responseJson = try JSONSerialization.jsonObject(with: response.data!) as? [String: Any]
                let data: [[String: Any]] = responseJson!["data"] as! [[String: Any]]
                
                var taskModelList = [TaskModel]()
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
                
                data.forEach {
                    let taskModel = TaskModel(id: $0["id"] as! Int,
                                              title: $0["title"] as! String,
                                              message: $0["message"] as! String,
                                              date: dateFormatter.date(from: $0["date"] as! String) ?? Date())
                    taskModelList.append(taskModel)
                }
                
                successHandler(taskModelList)
            } catch {
                failedHandler("Failed to return data")
            }
        }
    }
}
